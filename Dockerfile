FROM openjdk:8
LABEL author="DevOps"
LABEL version="1.0"
COPY ./target/spring-petclinic-4.2.6-SNAPSHOT.jar /spring-petclinic-4.2.6-SNAPSHOT.jar
EXPOSE 8000
ENTRYPOINT ["java", "-jar","/spring-petclinic-4.2.6-SNAPSHOT.jar"] 
#CMD ["/spring-petclinic-4.2.6-SNAPSHOT.jar"]
